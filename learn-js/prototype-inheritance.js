// ЗАДАЧА 1
// Задача состоит из двух частей.

// У нас есть объекты:

// let head = {
//   glasses: 1
// };

// let table = {
//   pen: 3
// };

// let bed = {
//   sheet: 1,
//   pillow: 2
// };

// let pockets = {
//   money: 2000
// };
// С помощью свойства __proto__ задайте прототипы так, 
// чтобы поиск любого свойства выполнялся по следующему 
// пути: pockets → bed → table → head. Например, pockets.pen 
// должно возвращать значение 3 (найденное в table), а 
// bed.glasses – значение 1 (найденное в head).
// Ответьте на вопрос: как быстрее получить значение 
// glasses – через pockets.glasses или через head.glasses? 
// При необходимости составьте цепочки поиска и сравните их.

console.log('ЗАДАЧА 1');

let head = {
    glasses: 1
};
  
let table = {
    pen: 3,
    __proto__: head,
};
  
let bed = {
    sheet: 1,
    pillow: 2,
    __proto__: table,
};
  
let pockets = {
    money: 2000,
    __proto__: bed,
};

console.log(pockets.pen); // 3

// Быстрее получить значение glasses через head, 
// потому что это свойство принадлежит обьекту, остальные 
// варианты будут содержать несколько переадресаций по обьектам


// ЗАДАЧА 2

// У нас есть два хомяка: шустрый (speedy) и ленивый (lazy); 
// оба наследуют от общего объекта hamster.

// Когда мы кормим одного хомяка, второй тоже наедается. 
// Почему? Как это исправить?

// let hamster = {
//   stomach: [],

//   eat(food) {
//     this.stomach.push(food);
//   }
// };

// let speedy = {
//   __proto__: hamster
// };

// let lazy = {
//   __proto__: hamster
// };

// // Этот хомяк нашёл еду
// speedy.eat("apple");
// console.log( speedy.stomach ); // apple

// // У этого хомяка тоже есть еда. Почему? Исправьте
// console.log( lazy.stomach ); // apple

console.log('ЗАДАЧА 2');

let hamster = {
    stomach: [],
  
    eat(food) {
      this.stomach.push(food);
    }
};
  
let speedy = {
    __proto__: hamster,
    stomach: [], //добавляем каждому собственный желудок
};
  
let lazy = {
    __proto__: hamster,
    stomach: [],
};
  
speedy.eat("apple");
console.log( speedy.stomach ); // [apple]

console.log( lazy.stomach ); // []   теперь все работает