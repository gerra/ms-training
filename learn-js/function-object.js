// ЗАДАЧА 1
// Измените код makeCounter() так, чтобы счётчик мог 
// увеличивать и устанавливать значение:

// counter() должен возвращать следующее значение (как и раньше).
// counter.set(value) должен устанавливать счётчику значение value.
// counter.decrease() должен уменьшать значение счётчика на 1.
// Посмотрите код из песочницы с полным примером использования.

// P.S. Для того, чтобы сохранить текущее значение счётчика, 
// можно воспользоваться как замыканием, так и свойством функции. 
// Или сделать два варианта решения: и так, и так.

let makeCounter = () => {
    let count = 0;
    let counter = () => {
        return count++;
    };
    counter.set = (value) => {
        count = value;
    }
    counter.decrease = () => {
        count--
    }
    return counter;
}
  
let counter = makeCounter();
counter();
counter();
counter();
counter();
counter();
counter();
console.log(counter()); // 6


// ЗАДАЧА 2
// Напишите функцию sum, которая бы работала следующим образом:

// sum(1)(2) == 3; // 1 + 2
// sum(1)(2)(3) == 6; // 1 + 2 + 3
// sum(5)(-1)(2) == 6
// sum(6)(-1)(-2)(-3) == 0
// sum(0)(1)(2)(3)(4)(5) == 15
// P.S. Подсказка: возможно вам стоит сделать особый метод 
// преобразования в примитив для функции.

console.log('ЗАДАЧА 2');

let sum = (a) => {
    return (b) => {
        if (isFinite(b)) {
            return sum(a + b);
        } 
        return a;
    }
};
console.log(sum(0)(1)(2)(3)(4)(5) == 15); // false
console.log(sum(0)(1)(2)(3)(4)(5)() == 15); // true
console.log(sum(1)(2)()); // 3
console.log(sum(5)(-1)(2)()); // 6
console.log(sum(6)(-1)(-2)(-3)()); // 0
console.log(sum(0)(1)(2)(3)(4)(5)()); // 15
console.log(sum(0)(1)(2)(3)(4)(5) (0)(1)(2)(3)(4)(5) (1)(2)(3)(4)(5) (1)(2)(3)(4)(5)()); // 60


// Смог найти только такое решение, чтобы прервать рекурсию
// необходимо в конце ряда аргументов передавать пустой ()