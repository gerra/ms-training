// // ЗАДАЧА 1
// // Преобразуйте user в JSON, затем прочитайте 
// // этот JSON в другую переменную.

console.log('ЗАДАЧА 1');

let user = {
    name: "Василий Иванович",
    age: 35
};

let json = JSON.stringify(user);
let newUser = JSON.parse(json);


// ЗАДАЧА 2
// В простых случаях циклических ссылок мы можем 
// исключить свойство, из-за которого они возникают, 
// из сериализации по его имени.

// Но иногда мы не можем использовать имя, так как 
// могут быть и другие, нужные, свойства с этим именем 
// во вложенных объектах. Поэтому можно проверять свойство по значению.

// Напишите функцию replacer для JSON-преобразования, 
// которая удалит свойства, ссылающиеся на meetup:

console.log('ЗАДАЧА 2');

let room = {
    number: 23
};
  
let meetup = {
    title: "Совещание",
    occupiedBy: [{name: "Иванов"}, {name: "Петров"}],
    place: room
};

room.occupiedBy = meetup;
meetup.self = meetup;

let json = JSON.stringify(meetup, replacer = (key, value) => {
    return key != '' && value == meetup
        ? undefined
        : value;
});

console.log(json);

//без промежуточных значений функции replacer():
// {"title":"Совещание","occupiedBy":[{"name":"Иванов"},{"name":"Петров"}],"place":{"number":23}}