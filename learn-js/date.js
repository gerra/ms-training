// ЗАДАЧА 1
// Создайте объект Date для даты: 
// 20 февраля 2012 года, 3 часа 12 минут. 
// Временная зона – местная.

// Для вывода используйте console.log.

console.log('ЗАДАЧА 1');

let date = new Date(2012, 1, 20, 3, 12);
console.log(date); // Mon Feb 20 2012 03:12:00 GMT+1100 (Владивосток, стандартное время)


// ЗАДАЧА 2
// Напишите функцию 
// getWeekDay(date), показывающую день недели в 
// коротком формате: «ПН», «ВТ», «СР», «ЧТ», «ПТ», 
// «СБ», «ВС».

console.log('ЗАДАЧА 2');

let getWeekDay = (date) => {
    switch(date.getDay()) {
        case 0:
            console.log('ВС');
            break;
        case 1:
            console.log('ПН');
            break;
        case 2:
            console.log('ВТ');
            break;
        case 3:
            console.log('СР');
            break;
        case 4:
            console.log('ЧТ');
            break;
        case 5:
            console.log('ПТ');
            break;
        case 6:
            console.log('СБ');
            break;
    }
}
getWeekDay(date); // ПН
date = new Date();
getWeekDay(date); // СБ


// ЗАДАЧА 3
// В Европейских странах неделя начинается с понедельника 
// (день номер 1), затем идёт вторник (номер 2) 
// и так до воскресенья (номер 7). Напишите функцию 
// getLocalDay(date), которая возвращает «европейский» 
// день недели для даты date.

console.log('ЗАДАЧА 3');

let getLocalDay = (date) => {
    let europeanWeekDays = [7, 1, 2, 3, 4, 5, 6];
    return europeanWeekDays[date.getDay()];
}
date = new Date(2012, 0, 3); 
console.log(getLocalDay(date)); // 2
date = new Date(); 
console.log(getLocalDay(date)); // 6


// ЗАДАЧА 4
// Создайте функцию getDateAgo(date, days), 
// возвращающую число, которое было days дней назад от даты date.

// К примеру, если сегодня двадцатое число, то 
// getDateAgo(new Date(), 1) вернёт девятнадцатое 
// и getDateAgo(new Date(), 2) – восемнадцатое.

// Функция должна надёжно работать при значении 
// days=365 и больших значениях
// P.S. Функция не должна изменять переданный ей объект date.

console.log('ЗАДАЧА 4');

let getDateAgo = (date, days) => {
    let dateClone = new Date(date);
    return new Date(dateClone.setDate(date.getDate() - days)).getDate();
}

let date = new Date(2015, 0, 2);
console.log(getDateAgo(date, 1)); // 1, (1 Jan 2015)
console.log(getDateAgo(date, 2)); // 31, (31 Dec 2014)
console.log(getDateAgo(date, 365)); // 2, (2 Jan 2014)


// ЗАДАЧА 5
// Напишите функцию getLastDayOfMonth(year, month), 
// возвращающую последнее число месяца. Иногда это 
// 30, 31 или даже февральские 28/29.

// Параметры:

// year – год из четырёх цифр, например, 2012.
// month – месяц от 0 до 11.
// К примеру, getLastDayOfMonth(2012, 1) = 29 
// (високосный год, февраль).

console.log('ЗАДАЧА 5');

let getLastDayOfMonth = (year, month) => {
    let date = new Date(year, month + 1, 0);
    return date.getDate();
}

console.log(getLastDayOfMonth(2012, 0)); // 31
console.log(getLastDayOfMonth(2012, 1)); // 29
console.log(getLastDayOfMonth(2013, 1)); // 28


// ЗАДАЧА 6
// Напишите функцию getSecondsToday(), возвращающую 
// количество секунд с начала сегодняшнего дня.

// Функция должна работать в любой день, т.е. в 
// ней не должно быть конкретного значения сегодняшней даты.

console.log('ЗАДАЧА 6');

let getSecondsToday = () => {
    let date = new Date();
    return (new Date() - date.setHours(0, 0, 0)) / 1000;
}

console.log(`С начала дня прошло ${getSecondsToday()} секунд`); // 73869
                                                                // (Время 20.30, т.е 20,5 * 3600)


// ЗАДАЧА 7
// Создайте функцию getSecondsToTomorrow(), 
// возвращающую количество секунд до завтрашней даты.

// P.S. Функция должна работать в любой день, 
// т.е. в ней не должно быть конкретного значения 
// сегодняшней даты.

console.log('ЗАДАЧА 7');

let getSecondsToTomorrow = () => {
    let tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    return new Date(tomorrow.setHours(0, 0, 0,) - new Date()) / 1000
}

console.log(getSecondsToTomorrow()) // 11223


// ЗАДАЧА 8
// Напишите функцию formatDate(date), форматирующую 
// date по следующему принципу:

// Если спустя date прошло менее 1 секунды, 
// вывести "прямо сейчас".
// В противном случае, если с date прошло меньше 1 
// минуты, вывести "n сек. назад".
// В противном случае, если меньше часа, вывести "m мин. назад".
// В противном случае, полная дата в формате "DD.MM.YY HH:mm". 
// А именно: "день.месяц.год часы:минуты", всё в виде двух 
// цифр, т.е. 31.12.16 10:00.

console.log('ЗАДАЧА 8');

let formatDate = (date) => {
    let diff = new Date().getTime() - date.getTime();

    let arr = date;
    arr = [
        `0${arr.getDate()}`,
        `0${arr.getMonth() + 1}`,
        `${arr.getFullYear()}`,
        `0${arr.getHours()}`,
        `0${arr.getMinutes()}`,
    ].map(element => element.slice(-2));
    return diff / 1000 <= 1
        ? 'прямо сейчас'
        : diff / 1000 <= 60
            ? `${diff / 1000} секунд назад`
            : diff / 1000 <= 3600
                ? `${diff / 60000} мин назад`
                : `${arr.slice(0, 3).join('.')} ${arr.slice(3).join(':')}`;
}

console.log(formatDate(new Date(new Date - 1))); // прямо сейчас
console.log(formatDate(new Date(new Date - 30 * 1000))); // 30 секунд назад
console.log(formatDate(new Date(new Date - 5 * 60 * 1000))); // 5 мин назад
// вчерашняя дата
console.log(formatDate(new Date(new Date - 86400 * 1000))); // 09.10.21 21:50