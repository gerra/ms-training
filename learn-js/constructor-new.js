// ЗАДАЧА 1
// Возможно ли создать функции A и B в примере ниже, где объекты равны new A()==new B()?

// function A() { ... }
// function B() { ... }

// let a = new A;
// let b = new B;

// alert( a == b ); // true
// Если да – приведите пример вашего кода.

console.log('ЗАДАЧА 1');

let someObj = {};
function A() {
    return someObj;
}
function B() {
    return someObj;
}

let a = new A;
let b = new B;

alert(a == b);


// ЗАДАЧА 2
// Создайте функцию-конструктор Calculator, 
// который создаёт объекты с тремя методами:

// read() запрашивает два значения при помощи prompt 
// и сохраняет их значение в свойствах объекта.
// sum() возвращает сумму введённых свойств.
// mul() возвращает произведение введённых свойств.

console.log('ЗАДАЧА 2');

function Calculator() {
    this.value1 = 0,
    this.value2 = 0,
    this.read = function() {
        this.value1 = +prompt('Введите первое число', 0);
        this.value2 = +prompt('Введите второе число', 0);
    },
    this.sum = function() {
        return this.value1 + this.value2;
    }
    this.mul = function() {
        return this.value1 * this.value2;
    }
}


// ЗАДАЧА 3
// Напишите функцию-конструктор Accumulator(startingValue).

// Объект, который она создаёт, должен уметь следующее:

// Хранить «текущее значение» в свойстве value. 
// Начальное значение устанавливается в аргументе 
// конструктора startingValue.
// Метод read() использует prompt для получения числа 
// и прибавляет его к свойству value.
// Таким образом, свойство value является текущей суммой всего, 
// что ввёл пользователь при вызовах метода read(), 
// с учётом начального значения startingValue.

console.log('ЗАДАЧА 3');

function Accumulator(startingValue) {
    this.value = startingValue,
    this.read = function() {
        this.value += +prompt('Любое число', 0);
    };
}

let acc = new Accumulator(15);
acc.read();
console.log(acc.value);