// // ЗАДАЧА 1
// // Создайте декоратор spy(func), который должен возвращать 
// // обёртку, которая сохраняет все вызовы функции в своём 
// // свойстве calls.

// // Каждый вызов должен сохраняться как массив аргументов.

// console.log('ЗАДАЧА 1');

// let spy = (func) => {
//     wrapper.calls = [];
//     return function wrapper(...args) {
//         wrapper.calls.push(args);
//         return func.apply(this, args)
//     }
// }

// function work(a, b) {
//     console.log(a + b);
// }
  
// work = spy(work);
  
// work(1, 2); // 3
// work(4, 5); // 9
  
// for (let args of work.calls) {
//     console.log(`call: ${args.join()}`); // "call:1,2", "call:4,5"
// }


// // ЗАДАЧА 2
// // Создайте декоратор delay(f, ms), который 
// // задерживает каждый вызов f на ms миллисекунд.
// // Другими словами, delay(f, ms) возвращает 
// // вариант f с «задержкой на ms мс».

// console.log('ЗАДАЧА 2');

// let delay = (f, ms) => {
//     let wrapper = (...args) => {
//         setTimeout(() => f.apply(this, args), ms)
//     }
//     return wrapper;
// }

// let f = (x) => {
//     console.log(x);
// }
//  // создаём обёртки
// let f1000 = delay(f, 1000);
// let f1500 = delay(f, 1500);
// let f3000 = delay(f, 3000);
  
// f1000('test'); // показывает "test" после 1000 мс
// f1500('test'); // показывает "test" после 1500 мс
// f3000('test'); // показывает "test" после 3000 мс


// // ЗАДАЧА 3
// // Результатом декоратора debounce(f, ms) должна быть 
// // обёртка, которая передаёт вызов f не более одного 
// // раза в ms миллисекунд. Другими словами, когда мы 
// // вызываем debounce, это гарантирует, что все остальные 
// // вызовы будут игнорироваться в течение ms.

// // На практике debounce полезен для функций, которые 
// // получают/обновляют данные, и мы знаем, что повторный 
// // вызов в течение короткого промежутка времени не даст 
// // ничего нового. Так что лучше не тратить на него ресурсы.

// console.log('ЗАДАЧА 3');

// let debounce = (f, ms) => {
//     let timeout = false;
//     return function wrapper(...args) {
//         if (timeout) {
//             return;
//         };
//         f.apply(this, args);
//         timeout = true
//         setTimeout(() => timeout = false, ms);
//     }
// }

// let f = debounce(console.log, 1000);

// f(1);                        // выполняется немедленно
// f(2);                         // проигнорирован
// setTimeout( () => f(3), 100); // проигнорирован (прошло только 100 мс)
// setTimeout( () => f(4), 1100); // выполняется
// setTimeout( () => f(5), 1500); // проигнорирован


// ЗАДАЧА 4
// Создайте «тормозящий» декоратор throttle(f, ms), который 
// возвращает обёртку, передавая вызов в f не более одного 
// раза в ms миллисекунд. Те вызовы, которые попадают в 
// период «торможения», игнорируются.

// Отличие от debounce – если проигнорированный вызов 
// является последним во время «задержки», то он выполняется в конце.

console.log('ЗАДАЧА 4');

let throttle = (f, ms) => {
    let timeout = 0;
    let calls = [];
    return function wrapper(...args) {
        if (timeout) {
            calls.push(...args);
            return
        }
        f.apply(this, args);
        timeout = ms;
        setTimeout(
            () => {
                timeout = 0
                if (calls) {
                    wrapper.apply(this, args);
                    calls.length = 0;
            }
        },
        ms
        )
    }
}

function f(a) {
    console.log(a)
}

// f1000 передаёт вызовы f максимум раз в 1000 мс
let f1000 = throttle(f, 1000);
let f2000 = throttle(f, 2000);

f1000(1); // показывает 1 через каждые 1000мс (так ведь и должно быть? :) )
f1000(2); // (ограничение, 1000 мс ещё нет)
f1000(3); // (ограничение, 1000 мс ещё нет)
