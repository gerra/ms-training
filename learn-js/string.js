// ЗАДАЧА 1
// Напишите функцию ucFirst(str), 
// возвращающую строку str с заглавным первым символом

console.log('ЗАДАЧА 1');

function ucFirst(str) {
    return str[0].toUpperCase() + str.slice[1];
}


// ЗАДАЧА 2
// Напишите функцию checkSpam(str), возвращающую true, 
// если str содержит 'viagra' или 'XXX', а иначе false.

// Функция должна быть нечувствительна к регистру

console.log('ЗАДАЧА 2');

function checkSpam(str) {
    let newStr = str.toUpperCase();
    return newStr.includes('VIAGRA') || newStr.includes('XXX');
}


// ЗАДАЧА 3
// Создайте функцию truncate(str, maxLength), 
// которая проверяет длину строки str и, если 
// она превосходит maxLength, заменяет конец str 
// на "…", так, чтобы её длина стала равна maxLength.

// Результатом функции должна быть та же строка, 
// если усечение не требуется, либо, если необходимо, 
// усечённая строка.

console.log('ЗАДАЧА 3');

function truncate(str, maxLength) {
    return str.length > maxLength
        ? `${str.slice(0, maxLength - 3)}...`
        : str;
}


// ЗАДАЧА 4
// Есть стоимость в виде строки "$120". То есть сначала 
// идёт знак валюты, а затем – число.

// Создайте функцию extractCurrencyValue(str), 
// которая будет из такой строки выделять числовое 
// значение и возвращать его.

console.log('ЗАДАЧА 4');

function extractCurrencyValue(str) {
    return newStr = +str.slice(1);
}