// ЗАДАЧА 1
// Напишите функцию sumTo(n), которая вычисляет 
// сумму чисел 1 + 2 + ... + n.
// Сделайте три варианта решения:

// С использованием цикла.
// Через рекурсию, т.к. sumTo(n) = n + sumTo(n-1) for n > 1.
// С использованием формулы арифметической прогрессии.

// P.S. Какой вариант решения самый быстрый? Самый медленный? Почему?

// P.P.S. Можно ли при помощи рекурсии посчитать sumTo(100000)?

console.log('ЗАДАЧА 1.1');

let start = new Date();
let sumTo = (n) => {
    let sum = 0;
    for (let i = 1; i <= n; i++) {
        sum += i;
    }
    return sum;
}

console.log(sumTo(5000)); // 12502500 
console.log(`Время выполнения ${new Date().getTime() - start.getTime()}`); // Время выполнения 1

console.log('ЗАДАЧА 1.2');

let start = new Date();
let sumTo = (n) => {
    let sum = 1;
    return n === 1
        ? n
        : sum = n + sumTo(n-1);
}

console.log(sumTo(5000)); // 12502500
console.log(`Время выполнения ${new Date().getTime() - start.getTime()}`); // Время выполнения 2


console.log('ЗАДАЧА 1.3');

let start = new Date();
let sumTo = (n) => {
    return n === 1
        ? n
        : (1 + n) * n / 2
}

console.log(sumTo(100000)); // 5000050000
console.log(`Время выполнения ${new Date().getTime() - start.getTime()}`); // Время выполнения 1

// Быстрее всего будет работать третий вариант, 
// потому что нет перебора и вложенных вызовов
// Первый и второй вариант по число 5к, третий 100к, 
// то есть время выполнения намного меньше
// Так же рекурсия у меня упала уже примерно при 6к вызовов, 
// а не при 10к как в учебнике


// ЗАДАЧА 2
// Задача – написать функцию factorial(n), 
// которая возвращает n!, используя рекурсию.

console.log('ЗАДАЧА 2');

let factorial = (n) => {
    return n === 1
        ? n
        : n * factorial(n - 1);
}
console.log(factorial(5));


// ЗАДАЧА 3
// Напишите функцию fib(n) которая возвращает n-е 
// число Фибоначчи.
// P.S. Все запуски функций из примера выше должны 
// работать быстро. Вызов fib(77) должен занимать не 
// более доли секунды.

console.log('ЗАДАЧА 3');

let startOne = new Date();
let fibOne = (n) => {
    let a = 1;
    let b = 1;
    for (let i = 2; i < n; i++) {
        let c = a + b;
        a = b;
        b = c;
    }
    console.log(`Время выполнения ${new Date().getTime() - startOne.getTime()}`); // Время выполнения 0
    return b;
}

// ИЛИ по формуле Бине

let startTwo = new Date();
let fibTwo = (n) => {
    let index = Math.pow(5, 0.5);

    let left = (1 + index) / 2;
    let right = (1 - index) / 2;

    console.log(`Время выполнения ${new Date().getTime() - startTwo.getTime()}`); // Время выполнения 1
    return Math.round((Math.pow(left, n) -  Math.pow(right, n)) / index);
}

console.log(fibOne(15)); // 610
console.log(fibOne(77)); // 5527939700884757

console.log(fibTwo(15)); // 610
console.log(fibTwo(77)); // 5527939700884771

// Вывод: по формуле Бине код немного короче, 
// но при больших значениях
// результат некорректный из-за ошибки вычисления дробных чисел


// ЗАДАЧА 4
// Допустим, у нас есть односвязный список
// Напишите функцию printList(list), которая 
// выводит элементы списка по одному.

console.log('ЗАДАЧА 4');

let printList = (list) => {
    console.log(list.value);
    if (list.next) {
        printList(list.next);
    }
}

let list = {
    value: 1,
    next: {
        value: 2,
        next: {
            value: 3,
            next: {
                value: 4,
                next: null
            }
        }
    }
};

printList(list); // 1 2 3 4


// ЗАДАЧА 5

// Выведите односвязный список из предыдущего 
// задания Вывод односвязного списка в обратном порядке.

let list = {
    value: 1,
    next: {
        value: 2,
        next: {
            value: 3,
            next: {
                value: 4,
                next: null
            }
        }
    }
};

let printReverseList = (list) => {
        if (list.next) {
            printReverseList(list.next);
        }
        console.log(list.value)
}

printReverseList(list); // 4 3 2 1