// ЗАДАЧА 1
// Есть объект salaries с произвольным количеством 
// свойств, содержащих заработные платы.

// Напишите функцию sumSalaries(salaries), которая 
// возвращает сумму всех зарплат с помощью метода 
// Object.values и цикла for..of.

// Если объект salaries пуст, то результат 
// должен быть 0.

console.log('ЗАДАЧА 1');

let salaries = {
    "John": 100,
    "Pete": 300,
    "Mary": 250,
};

let sumSalaries = (salaries) => {
    let sum = 0;
    Object.values(salaries).forEach(value => {
        sum += value;
    });

    return sum;
}

console.log(sumSalaries(salaries)); // 650


// ЗАДАЧА 2
// Напишите функцию count(obj), которая 
// возвращает количество свойств объекта
// Постарайтесь сделать код как можно короче.
// P.S. Игнорируйте символьные свойства, 
// подсчитывайте только «обычные».

console.log('ЗАДАЧА 2');

let user = {
  name: 'John',
  age: 30
};

let count = (obj) => {
    return Object.entries(obj).length;
}
console.log(count(user)); // 2
console.log(count(salaries)); // 3