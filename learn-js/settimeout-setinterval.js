// ЗАДАЧА 1
// Напишите функцию printNumbers(from, to), которая 
// выводит число каждую секунду, начиная от from и заканчивая to.

// Сделайте два варианта решения.

// Используя setInterval.
// Используя рекурсивный setTimeout.

console.log('ЗАДАЧА 1.1');

let printNumbers = (from, to) => {
    let timerId = setInterval(() => {
        console.log(from), 100;
        if (from >= to) {
            clearInterval(timerId);
        }
        from++
    }, 1000);
}

printNumbers(0, 3) // 0, 1, 2, 3

console.log('ЗАДАЧА 1.2');

let printNumbers = (from, to) => {
    let timerId = setTimeout(delay = () => {
        console.log(from++);
        if (from <= to) {
            timerId = setTimeout(delay, 1000)
        }
    }, 1000)
}

printNumbers(0, 3) // 0, 1, 2, 3