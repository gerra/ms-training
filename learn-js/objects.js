// ЗАДАЧА 1
// Напишите код, выполнив задание из каждого пункта отдельной строкой:

// 1.Создайте пустой объект user.
// 2.Добавьте свойство name со значением John.
// 3.Добавьте свойство surname со значением Smith.
// 4.Измените значение свойства name на Pete.
// 5.Удалите свойство name из объекта.

console.log('ЗАДАЧА 1');

let user = {};
user.name = 'John';
user.surname = 'Smith';
user.name = 'Pete';
delete user.name;


// ЗАДАЧА 2
// Напишите функцию isEmpty(obj), которая возвращает true, 
// если у объекта нет свойств, иначе false.
// Должно работать так:

// let schedule = {};
// alert( isEmpty(schedule) ); // true
// schedule["8:30"] = "get up";
// alert( isEmpty(schedule) ); // false

console.log('ЗАДАЧА 2');

function isEmpty(obj) {
    for (let prop in obj) {
        return false;
    }
    return true;
}


// ЗАДАЧА 3
// У нас есть объект, в котором хранятся зарплаты нашей команды:

let salaries = {
  John: 100,
  Ann: 160,
  Pete: 130
}
// Напишите код для суммирования всех зарплат и сохраните
// результат в переменной sum. Должно получиться 390.
// Если объект salaries пуст, то результат должен быть 0.

console.log('ЗАДАЧА 3');

let sum = 0;
for (let key in salaries) {
    if (isEmpty(salaries)) {
        break;
    }
    sum += salaries[key];
}


// ЗАДАЧА 4
// Создайте функцию multiplyNumeric(obj), которая умножает 
// все числовые свойства объекта obj на 2.
// Обратите внимание, что multiplyNumeric не нужно ничего возвращать. 
// Следует напрямую изменять объект.
// P.S. Используйте typeof для проверки, что значение свойства числовое.

console.log('ЗАДАЧА 4');

function multiplyNumeric(obj) {
    for (let key in obj) {
        if (typeof[key] === 'number') {
            obj[key] *= 2;
        } else continue;
    }
}

