// ЗАДАЧА 1
// Напишите функцию camelize(str), которая преобразует
// строки вида «my-short-string» в «myShortString».

// То есть дефисы удаляются, а все слова после них
// получают заглавную букву.
// P.S. Подсказка: используйте split, чтобы разбить
// строку на массив символов, потом переделайте всё
// как нужно и методом join соедините обратно.

console.log('ЗАДАЧА 1');

function camelize(str) {
    let arr = str.split('-');
    
    let uppercased = arr.map((item, index) =>
            index == 0
                ? item
                : item[0].toUpperCase() + item.slice(1)
        );
    
        return uppercased.join('');
}

console.log(camelize("background-color")); //backgroundColor
console.log(camelize("list-style-image")); //listStyleImage
console.log(camelize("-webkit-transition")); //WebkitTransition


// ЗАДАЧА 2
// Напишите функцию filterRange(arr, a, b),
// которая принимает массив arr, ищет в нём элементы
// между a и b и отдаёт массив этих элементов.

// Функция должна возвращать новый массив и не изменять
//  исходный.

console.log('ЗАДАЧА 2');

function filterRange(arr, a, b) {
    
    return arr.filter((item) =>
        item > a && item < b);
}

let arr = [5, 3, 8, 1];
let filtered = filterRange(arr, 1, 4);

console.log(filtered); // 3 (совпадающие значения)
console.log(arr); // [5, 3, 8, 1]


// ЗАДАЧА 3
// Напишите функцию filterRangeInPlace(arr, a, b), 
// которая принимает массив arr и удаляет из него все 
// значения кроме тех, которые находятся между a и b. 
// То есть, проверка имеет вид a ≤ arr[i] ≤ b.

// Функция должна изменять принимаемый массив и ничего не возвращать.

console.log('ЗАДАЧА 3');

function filterRangeInPlace(arr, a, b) {
    
    for (let i = 0; i < arr.length; i++) {
        let item = arr[i];
        if (item < a || item > b) {
            arr.splice(i, 1);
            i--;
        }      
    }
}

let arr = [5, 3, 8, 1];

filterRangeInPlace(arr, 1, 4); // удалены числа вне диапазона 1..4
console.log(arr); // [3, 1]


// ЗАДАЧА 4
// Сортировать в порядке по убыванию

// let arr = [5, 2, 1, -10, 8];

// // ... ваш код для сортировки по убыванию

// alert( arr ); // 8, 5, 2, 1, -10

console.log('ЗАДАЧА 4');

let arr = [5, 2, 1, -10, 8];

arr.sort((a,b) => b - a);

console.log(arr); // [8, 5, 2, 1, -10]



// ЗАДАЧА 5
// У нас есть массив строк arr. Нужно получить 
// отсортированную копию, но оставить arr неизменённым.

// Создайте функцию copySorted(arr), которая 
// будет возвращать такую копию.
 
console.log('ЗАДАЧА 5');

function copySorted(arr) {
    newArr = arr.slice();
    
    return newArr.sort();
}

let arr = ["HTML", "JavaScript", "CSS"];
let sorted = copySorted(arr);

console.log(sorted); // CSS, HTML, JavaScript
console.log(arr); // ['HTML', 'JavaScript', 'CSS']


// ЗАДАЧА 6.1,
// Создайте функцию конструктор Calculator, которая 
// создаёт «расширяемые» объекты калькулятора.

// Реализуйте метод calculate(str), который 
// принимает строку типа "1 + 2" в формате 
// «ЧИСЛО оператор ЧИСЛО» (разделено пробелами) и 
// возвращает результат. Метод должен понимать 
// плюс + и минус -.

// ЗАДАЧА 6.2
// Затем добавьте метод addMethod(name, func), который 
// добавляет в калькулятор новые операции. Он принимает 
// оператор name и функцию с двумя аргументами func(a,b), 
// которая описывает его.

// Например, давайте добавим умножение *, деление / 
// и возведение в степень **:

console.log('ЗАДАЧА 6');

function Calculator() {
    this.operators = {
        "-": (a, b) => a - b,
        "+": (a, b) => a + b
    };

    this.calculate = function(str) {
        let arr = str.split(' ');
        let a = +arr[0];
        let op = arr[1];
        let b = +arr[2];
        
        return this.operators[op](a, b);
    }

    this.addMethod = function(name, func) {
        this.operators[name] = func;
      };
}

//Результат задачи 6.1
let calc = new Calculator;
console.log(calc.calculate("3 + 7")); // 10

//Результат задачи 6.2
let powerCalc = new Calculator;
powerCalc.addMethod("*", (a, b) => a * b);
powerCalc.addMethod("/", (a, b) => a / b);
powerCalc.addMethod("**", (a, b) => a ** b);

let result = powerCalc.calculate("2 ** 3");
console.log(result); // 8


// ЗАДАЧА 7
// У вас есть массив объектов 
// user, и в каждом из них есть user.name. 
// Напишите код, который преобразует их в массив имён.

console.log('ЗАДАЧА 7');

let vasya = {
    name: "Вася",
    age: 25
};
let petya = {
    name: "Петя",
    age: 30
};
let masha = {
    name: "Маша",
    age: 28
};
let users = [vasya, petya, masha];
let names = users.map(users => users.name);

console.log(names); // ['Вася', 'Петя', 'Маша']


// ЗАДАЧА 8
// У вас есть массив объектов user, и у 
// каждого из объектов есть name, surname и id.

// Напишите код, который создаст ещё один массив 
// объектов с параметрами id и fullName, где 
// fullName – состоит из name и surname.    
// Итак, на самом деле вам нужно трансформировать 
// один массив объектов в другой. Попробуйте 
// использовать =>. Это небольшая уловка.

console.log('ЗАДАЧА 8');

let users = [
        {
            name : 'James',
            surname : 'Potter',
            id : 1,
        },
        {
            name : 'Ron',
            surname : 'Weasley',
            id : 2,
        },
        {
            name : 'Nevill',
            surname : 'Longbottom',
            id : 3,
        }
];

function renameArrayItems(arr) {
    return users.map(item => ({
        fullName : `${item.name} ${item.surname}`,
        id : item.id
    }));
}

console.log(renameArrayItems(users));   // 0: {fullName: 'James Potter', id: 1}
                                        // 1: {fullName: 'Ron Weasley', id: 2}
                                        // 2: {fullName: 'Nevill Longbottom', id: 3}


// ЗАДАЧА 9
// Напишите функцию sortByAge(users), 
// которая принимает массив объектов со свойством 
// age и сортирует их по нему.

console.log('ЗАДАЧА 9');

function sortByAge(users) {  
    users.sort((a, b) => a.age - b.age);
}

let vasya = { name: "Вася", age: 25 };
let petya = { name: "Петя", age: 30 };
let masha = { name: "Маша", age: 28 };
let arr = [ vasya, petya, masha ];

sortByAge(arr);

console.log(arr[0].name); // Вася
console.log(arr[1].name); // Маша
console.log(arr[2].name); // Петя


// ЗАДАЧА 10

// Напишите функцию shuffle(array), которая 
// перемешивает (переупорядочивает случайным образом) 
// элементы массива.

// Многократные прогоны через shuffle могут привести 
// к разным последовательностям элементов. Например:

// Все последовательности элементов должны иметь 
// одинаковую вероятность. Например, [1,2,3] может 
// быть переупорядочено как [1,2,3] или [1,3,2], или 
// [3,1,2] и т.д., с равной вероятностью каждого случая.

console.log('ЗАДАЧА 10');

function shuffle(array) {
    array.sort(() => Math.round(Math.random() * 100) - 50);
    
    return array;
}

let arr = [1, 2, 3];
console.log(shuffle(arr)); // [1, 2, 3]
console.log(shuffle(arr)); // [1, 2, 3]
console.log(shuffle(arr)); // [3, 2, 1]
console.log(shuffle(arr)); // [2, 3, 1]
console.log(shuffle(arr)); // [1, 3, 2]


// ЗАДАЧА 11
// Напишите функцию getAverageAge(users),
// которая принимает массив объектов со 
// свойством age и возвращает средний возраст.

console.log('ЗАДАЧА 11');

function getAverageAge(users) {   
    let averageAge = users.reduce((sum, user) => sum + user.age, 0);
    
    return averageAge /= users.length;
}
let vasya = { name: "Вася", age: 25 };
let petya = { name: "Петя", age: 30 };
let masha = { name: "Маша", age: 29 };

let arr = [ vasya, petya, masha ];

console.log(getAverageAge(arr)); // 28

// ЗАДАЧА 12
// Пусть arr – массив строк.

// Напишите функцию unique(arr), которая 
// возвращает массив, содержащий только уникальные 
// элементы arr.

console.log('ЗАДАЧА 12');

function unique(arr) {
    let arrOfUnique = [];
    
    arr.forEach((item) => {
        if (!arrOfUnique.includes(item)) {
            arrOfUnique.push(item);
        }});

    return arrOfUnique;
}

let strings = ["кришна", "кришна", "харе", "харе",
  "харе", "харе", "кришна", "кришна", ":-O"
];

console.log(unique(strings)); // ['кришна', 'харе', ':-O']