// ЗАДАЧА 1
// Создайте скрипт, который запрашивает ввод двух 
// чисел (используйте prompt) и после показывает их сумму.

console.log('ЗАДАЧА 1');

let value1 = +prompt('val1?');
let value2 = +prompt('val2?');
console.log(value1 + value2);


// ЗАДАЧА 2
// Методы Math.round и toFixed, согласно документации, 
// округляют до ближайшего целого числа: 0..4 округляется 
// в меньшую сторону, тогда как 5..9 в большую сторону.
// Например:

// alert( 1.35.toFixed(1) ); // 1.4
// Но почему в примере ниже 6.35 округляется до 6.3?

// alert( 6.35.toFixed(1) ); // 6.3
// Как правильно округлить 6.35?

console.log('ЗАДАЧА 2');

console.log((Math.round(6.35 * 10)) / 10);


// ЗАДАЧА 3
// Создайте функцию readNumber, которая будет запрашивать 
// ввод числового значения до тех пор, 
// пока посетитель его не введёт.

// Функция должна возвращать числовое значение.

// Также надо разрешить пользователю остановить процесс ввода, 
// отправив пустую строку или нажав «Отмена». 
// В этом случае функция должна вернуть null.

console.log('ЗАДАЧА 3');

function readNumber() {
    let number;

    do {
        number = prompt("Число?");
    } while (!isFinite(number));

    if (number === null || number === '') {
        return null
    }
    
    return number;
}

console.log(readNumber());


// ЗАДАЧА 4
// // Встроенный метод Math.random() возвращает 
// случайное число от 0 (включительно) до 1 (но не включая 1)

// Напишите функцию random(min, max), которая генерирует 
// случайное число с плавающей точкой от min до max 
// (но не включая max).

console.log('ЗАДАЧА 4');

function random(min, max) {
    return (max - min) * Math.random() + min;
}

console.log(random(15, 20));


// ЗАДАЧА 5
// Напишите функцию randomInteger(min, max), которая генерирует 
// случайное целое (integer) число от min до max (включительно).

// Любое число из интервала min..max должно появляться с 
// одинаковой вероятностью.

console.log('ЗАДАЧА 5');

function randomInteger(min, max) {
    return Math.floor((max - min + 1) * Math.random() + min)
}
console.log(randomInteger(15, 20));