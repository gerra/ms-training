// ЗАДАЧА 1
// Допустим, у нас есть массив arr.
// Создайте функцию unique(arr), которая вернёт 
// массив уникальных, не повторяющихся значений массива arr.

// P.S. Здесь мы используем строки, но значения могут быть любого типа.
// P.P.S. Используйте Set для хранения уникальных значений.

console.log('ЗАДАЧА 1');

let unique = (arr) => {
    return new Set(arr);
}
let values = ["Hare", "Krishna", "Hare", "Krishna",
  "Krishna", "Krishna", "Hare", "Hare", ":-O"
];

console.log(unique(values)); // Set(3) {'Hare', 'Krishna', ':-O'}


// ЗАДАЧА 2
// Отфильтруйте анаграммы
// Напишите функцию aclean(arr), которая возвращает 
// массив слов, очищенный от анаграмм.
// Из каждой группы анаграмм должно остаться 
// только одно слово, не важно какое.

console.log('ЗАДАЧА 2');

let arr = ["nap", "teachers", "cheaters",
    "PAN", "ear", "era", "hectares"];

let aclean = (arr) => {
    let map = new Map;
    arr.forEach(item => {
        let sorted = item.toLowerCase().split('').sort().join('');
        map.set(sorted, item);
    });
    
    return Array.from(map.values());
}

console.log(aclean(arr)); // ['PAN', 'hectares', 'era']
