// ЗАДАЧА 1
// Имеется объект dictionary, созданный с помощью 
// Object.create(null) для хранения любых пар ключ/значение.

// Добавьте ему метод dictionary.toString(), который 
// должен возвращать список ключей, разделённых запятой. 
// Ваш toString не должен выводиться при итерации объекта с помощью цикла for..in.

// Вот так это должно работать:

// let dictionary = Object.create(null);

// // ваш код, который добавляет метод dictionary.toString

// // добавляем немного данных
// dictionary.apple = "Apple";
// dictionary.__proto__ = "test"; // здесь __proto__ -- это обычный ключ

// // только apple и __proto__ выведены в цикле
// for(let key in dictionary) {
//   console.log(key); // "apple", затем "__proto__"
// }

// // ваш метод toString в действии
// console.log(dictionary); // "apple,__proto__"

console.log('ЗАДАЧА 1');

let dictionary = Object.create(null);

dictionary.toString = () => {
    let string = Object.keys(dictionary).join(', ');
    return string;
}

Object.defineProperty(dictionary, 'toString', {
    enumerable: false
});

// добавляем немного данных
dictionary.apple = "Apple";
dictionary.__proto__ = "test"; // здесь __proto__ -- это обычный ключ

// только apple и __proto__ выведены в цикле
for(let key in dictionary) {
    console.log(key); // "apple", затем "__proto__"
}

// метод toString в действии
console.log(dictionary); // "apple,__proto__"


// ЗАДАЧА 2
// Давайте создадим новый объект rabbit:

// function Rabbit(name) {
//   this.name = name;
// }
// Rabbit.prototype.sayHi = function() {
//   alert(this.name);
// };

// let rabbit = new Rabbit("Rabbit");
// Все эти вызовы делают одно и тоже или нет?

// rabbit.sayHi();
// Rabbit.prototype.sayHi();
// Object.getPrototypeOf(rabbit).sayHi();
// rabbit.__proto__.sayHi();

console.log('ЗАДАЧА 2');

function Rabbit(name) {
  this.name = name;
}
Rabbit.prototype.sayHi = function() {
  console.log(this.name);
};

let rabbit = new Rabbit("Rabbit");

rabbit.sayHi(); // вызывает наследованный метод sayHi() обьекта rabbit 
// вывод - Rabbit
Rabbit.prototype.sayHi(); // вызывает метод sayHi() конструктора Rabbit
// вывод - undefined(у конструктора не определено this.name)
Object.getPrototypeOf(rabbit).sayHi(); // вызывает метод sayHi() конструктора Rabbit
// вывод - undefined(у конструктора не определено this.name)
rabbit.__proto__.sayHi(); // вызывает метод sayHi() конструктора Rabbit
// вывод - undefined(у конструктора не определено this.name)