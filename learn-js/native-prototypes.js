// ЗАДАЧА 1
// Добавьте всем функциям в прототип метод defer(ms), 
// который вызывает функции через ms миллисекунд.

// После этого должен работать такой код:

// function f() {
//   alert("Hello!");
// }

// f.defer(1000); // выведет "Hello!" через 1 секунду

console.log('ЗАДАЧА 1');

if (!Function.prototype.defer) {
    Function.prototype.defer = function(ms) {
        setTimeout(this, ms);
    }
}

function f() {
    console.log('Hello!');
}
    
f.defer(1000); // выводит "Hello!" через 1 секунду


// ЗАДАЧА 2
// Добавьте всем функциям в прототип метод defer(ms), 
// который возвращает обёртку, откладывающую вызов функции на ms миллисекунд.

// Например, должно работать так:

// function f(a, b) {
//   alert( a + b );
// }

// f.defer(1000)(1, 2); // выведет 3 через 1 секунду.
// Пожалуйста, заметьте, что аргументы должны корректно передаваться оригинальной функции.

console.log('ЗАДАЧА 2');

Function.prototype.defer = function(ms) {
    return function wrapper(...args) {
        setTimeout(() => f.apply(this, args), ms);
    }
}

function f(a, b) {
    console.log(a + b);
}

f.defer(1000)(1, 2); // выводит 3 через 1 секунду.
f.defer(2500)(15, 5); // выводит 20 через 2,5 секунды.