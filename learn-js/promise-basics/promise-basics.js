// ЗАДАЧА 1
// Встроенная функция setTimeout использует колбэк-функции.
//  Создайте альтернативу, использующую промисы.

// Функция delay(ms) должна возвращать промис, который перейдёт 
// в состояние «выполнен» через ms миллисекунд, так чтобы мы могли добавить к нему .then:

// function delay(ms) {
//   // ваш код
// }

// delay(3000).then(() => alert('выполнилось через 3 секунды'));

console.log('ЗАДАЧА 1');

/**
 * Создает задержку в ms перед вызовом
 * @param {*} ms 
 */
function delay(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
    
delay(3000).then(() => alert('выполнилось через 3 секунды'));
delay(1000).then(() => alert('выполнилось через 1 секунды'));


// ЗАДАЧА 2
// Перепишите функцию showCircle, написанную в задании 
// Анимация круга с помощью колбэка таким образом, чтобы 
// она возвращала промис, вместо того чтобы принимать в аргументы функцию-callback.

// Новое использование:

// showCircle(150, 150, 100).then(div => {
//   div.classList.add('message-ball');
//   div.append("Hello, world!");
// });
// Возьмите решение из Анимация круга с помощью колбэка в качестве основы.

console.log('ЗАДАЧА 2');
/**
 * Добавляет в круг текст после завершения анимации
 */
function addText() {
    showCircle(150, 150, 100).then(div => {
        div.classList.add('message-ball');
        div.append("Hello, world!");
    });
}

/**
 * создает анимированный круг
 * @param {*} cx 
 * @param {*} cy 
 * @param {*} radius 
 */
function showCircle(cx, cy, radius) {
    let div = document.createElement('div');
    div.style.width = 0;
    div.style.height = 0;
    div.style.left = `${cx}px`;
    div.style.top = `${cy}px`;
    div.className = 'circle';
    document.body.append(div);

    return new Promise(resolve => {
        setTimeout(
            () => {
                div.style.width = `${radius * 2}px`;
                div.style.height = `${radius * 2}px`;

                div.addEventListener('transitionend', function handler() {
                    div.removeEventListener('transitionend', handler);
                    resolve(div);
                });
            }, 
            0
        );
    });
}