// ЗАДАЧА 1
// У нас есть объект:

// let user = {
//   name: "John",
//   years: 30
// };
// Напишите деструктурирующее присваивание, которое:

// свойство name присвоит в переменную name.
// свойство years присвоит в переменную age.
// свойство isAdmin присвоит в переменную 
// isAdmin (false, если нет такого свойства)

console.log('ЗАДАЧА 1');

let user = {
      name: "John",
      years: 30
};

let {name, years: age, isAdmin = false} = user;

console.log(name); // John
console.log(age); // 30
console.log(isAdmin); // false


// ЗАДАЧА 2
// У нас есть объект salaries с зарплатами:

// let salaries = {
//   "John": 100,
//   "Pete": 300,
//   "Mary": 250
// };
// Создайте функцию topSalary(salaries), 
// которая возвращает имя самого высокооплачиваемого сотрудника.

// Если объект salaries пустой, то нужно 
// вернуть null.
// Если несколько высокооплачиваемых сотрудников, 
// можно вернуть любого из них.
// P.S. Используйте Object.entries и деструктурирование, 
// чтобы перебрать пары ключ/значение.

console.log('ЗАДАЧА 2');

let salaries = {
      "John": 100,
      "Pete": 300,
      "Mary": 250,
};

let topSalary = (salaries) => {
    let topSalary = 0;
    let topName;

    if (!Object.entries(salaries).length) {
        return null;
    }

    Object.entries(salaries).forEach(user => {
        let [name, money] = user;
        if (money > topSalary) {
            topSalary = money;
            topName = name;
        }
    });
    
    return topName;
};

console.log(topSalary(salaries)); // Pete
